EESchema Schematic File Version 4
LIBS:UPS-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Raspberry Pi Zero (W) uHAT Template Board"
Date "2019-02-28"
Rev "1.0"
Comp ""
Comment1 "This Schematic is licensed under MIT Open Source License."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 5C77771F
P 5250 2950
F 0 "J1" H 5300 4067 50  0000 C CNN
F 1 "GPIO_CONNECTOR" H 5300 3976 50  0000 C CNN
F 2 "lib:PinSocket_2x20_P2.54mm_Vertical_Centered_Anchor" H 5250 2950 50  0001 C CNN
F 3 "~" H 5250 2950 50  0001 C CNN
	1    5250 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2450 4850 2450
Wire Wire Line
	4850 2450 4850 3250
Wire Wire Line
	5050 3250 4850 3250
Connection ~ 4850 3250
Wire Wire Line
	4850 3250 4850 3950
Wire Wire Line
	5050 3950 4850 3950
Connection ~ 4850 3950
Wire Wire Line
	4850 3950 4850 4100
Wire Wire Line
	5550 2250 5750 2250
Wire Wire Line
	5750 2250 5750 2650
Wire Wire Line
	5550 2650 5750 2650
Connection ~ 5750 2650
Wire Wire Line
	5750 2650 5750 2950
Wire Wire Line
	5550 2950 5750 2950
Connection ~ 5750 2950
Wire Wire Line
	5550 3450 5750 3450
Wire Wire Line
	5750 2950 5750 3450
Connection ~ 5750 3450
Wire Wire Line
	5750 3450 5750 3650
Wire Wire Line
	5550 3650 5750 3650
Connection ~ 5750 3650
Wire Wire Line
	5750 3650 5750 4100
Wire Wire Line
	5050 2850 4800 2850
Wire Wire Line
	5550 2050 5850 2050
Wire Wire Line
	5550 2150 5850 2150
Wire Wire Line
	5850 2150 5850 2050
Connection ~ 5850 2050
Text Notes 6400 2150 0    50   ~ 10
If back powering Pi with 5V \nNOTE that the Raspberry Pi 3B+ and Pi Zero \nand ZeroW do not include an input ZVD.
Wire Notes Line
	6350 1850 6350 2200
Wire Notes Line
	6350 2200 8200 2200
Wire Notes Line
	8200 2200 8200 1850
Wire Notes Line
	8200 1850 6350 1850
Wire Wire Line
	5850 2050 6050 2050
Text Label 4100 2150 0    50   ~ 0
GPIO2_SDA1
Text Label 4100 2250 0    50   ~ 0
GPIO3_SCL1
Text Label 4100 2350 0    50   ~ 0
GPIO4_GPIO_GCLK
Text Label 4100 2550 0    50   ~ 0
GPIO17_GEN0
Text Label 4100 2650 0    50   ~ 0
GPIO27_GEN2
Text Label 4100 2750 0    50   ~ 0
GPIO22_GEN3
Text Label 4100 2950 0    50   ~ 0
GPIO10_SPI_MOSI
Wire Wire Line
	4000 2950 5050 2950
Wire Wire Line
	4000 3050 5050 3050
Wire Wire Line
	4000 3150 5050 3150
Wire Wire Line
	4000 3350 5050 3350
Wire Wire Line
	4000 3450 5050 3450
Wire Wire Line
	4000 3550 5050 3550
Wire Wire Line
	4000 3650 5050 3650
Wire Wire Line
	4000 3750 5050 3750
Wire Wire Line
	4000 2750 5050 2750
Wire Wire Line
	4000 2650 5050 2650
Wire Wire Line
	4000 2550 5050 2550
Wire Wire Line
	4000 2350 5050 2350
Wire Wire Line
	4000 2250 5050 2250
Wire Wire Line
	4000 2150 5050 2150
Text Label 4100 3050 0    50   ~ 0
GPIO9_SPI_MISO
Text Label 4100 3150 0    50   ~ 0
GPIO11_SPI_SCLK
Text Label 4100 3350 0    50   ~ 0
ID_SD
Text Label 4100 3450 0    50   ~ 0
GPIO5
Text Label 4100 3550 0    50   ~ 0
GPIO6
Text Label 4100 3650 0    50   ~ 0
GPIO13
Text Label 4100 3750 0    50   ~ 0
GPIO19
Text Label 4100 3850 0    50   ~ 0
GPIO26
NoConn ~ 4000 2150
NoConn ~ 4000 2250
NoConn ~ 4000 2350
NoConn ~ 4000 2550
NoConn ~ 4000 2650
NoConn ~ 4000 2750
NoConn ~ 4000 2950
NoConn ~ 4000 3050
NoConn ~ 4000 3150
NoConn ~ 4000 3350
NoConn ~ 4000 3450
NoConn ~ 4000 3550
NoConn ~ 4000 3650
NoConn ~ 4000 3750
Text Label 5900 2350 0    50   ~ 0
GPIO14_TXD0
Text Label 5900 2450 0    50   ~ 0
GPIO15_RXD0
Text Label 5900 2550 0    50   ~ 0
GPIO18_GEN1
Text Label 5900 2750 0    50   ~ 0
GPIO23_GEN4
Text Label 5900 2850 0    50   ~ 0
GPIO24_GEN5
Text Label 5900 3050 0    50   ~ 0
GPIO25_GEN6
Text Label 5900 3150 0    50   ~ 0
GPIO8_SPI_CE0_N
Text Label 5900 3250 0    50   ~ 0
GPIO7_SPI_CE1_N
Wire Wire Line
	5550 3150 6600 3150
Wire Wire Line
	5550 3250 6600 3250
Text Label 5900 3350 0    50   ~ 0
ID_SC
Text Label 5900 3550 0    50   ~ 0
GPIO12
Text Label 5900 3750 0    50   ~ 0
GPIO16
Text Label 5900 3850 0    50   ~ 0
GPIO20
Text Label 5900 3950 0    50   ~ 0
GPIO21
Wire Wire Line
	5550 2350 6600 2350
Wire Wire Line
	5550 2450 6600 2450
Wire Wire Line
	5550 2550 6600 2550
Wire Wire Line
	5550 2750 6600 2750
Wire Wire Line
	5550 2850 6600 2850
Wire Wire Line
	5550 3050 6600 3050
Wire Wire Line
	5550 3350 6600 3350
Wire Wire Line
	5550 3550 6600 3550
Wire Wire Line
	5550 3750 6600 3750
Wire Wire Line
	5550 3850 6600 3850
NoConn ~ 6600 2350
NoConn ~ 6600 2450
NoConn ~ 6600 2550
NoConn ~ 6600 2750
NoConn ~ 6600 2850
NoConn ~ 6600 3050
NoConn ~ 6600 3150
NoConn ~ 6600 3250
NoConn ~ 6600 3350
NoConn ~ 6600 3550
NoConn ~ 6600 3750
NoConn ~ 6600 3850
NoConn ~ 6600 3950
Wire Wire Line
	5550 3950 6600 3950
$Comp
L Mechanical:MountingHole H1
U 1 1 5C7C4C81
P 8250 2600
F 0 "H1" H 8350 2646 50  0000 L CNN
F 1 "MountingHole" H 8350 2555 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 8250 2600 50  0001 C CNN
F 3 "~" H 8250 2600 50  0001 C CNN
	1    8250 2600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5C7C7FBC
P 8250 2800
F 0 "H2" H 8350 2846 50  0000 L CNN
F 1 "MountingHole" H 8350 2755 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 8250 2800 50  0001 C CNN
F 3 "~" H 8250 2800 50  0001 C CNN
	1    8250 2800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5C7C8014
P 8250 3000
F 0 "H3" H 8350 3046 50  0000 L CNN
F 1 "MountingHole" H 8350 2955 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 8250 3000 50  0001 C CNN
F 3 "~" H 8250 3000 50  0001 C CNN
	1    8250 3000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5C7C8030
P 8250 3200
F 0 "H4" H 8350 3246 50  0000 L CNN
F 1 "MountingHole" H 8350 3155 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 8250 3200 50  0001 C CNN
F 3 "~" H 8250 3200 50  0001 C CNN
	1    8250 3200
	1    0    0    -1  
$EndComp
$Sheet
S 1650 2800 650  800 
U 60AAF7A3
F0 "Battery Monitoring Subcircuit" 50
F1 "Batt Monitor.sch" 50
F2 "reg_in" I R 2300 2950 50 
F3 "bat_in" I R 2300 3100 50 
F4 "100%" O L 1650 2950 50 
F5 "80%" O L 1650 3050 50 
F6 "50%" O L 1650 3150 50 
F7 "30%" O L 1650 3250 50 
F8 "15%" O L 1650 3400 50 
F9 "shutdown" O R 2300 3350 50 
$EndSheet
$Sheet
S 1700 3950 600  800 
U 60AAFA27
F0 "LED Display Subcircuit" 50
F1 "LED Display.sch" 50
F2 "reg_in" I R 2300 4100 50 
F3 "full" I L 1700 4200 50 
F4 "80" I L 1700 4300 50 
F5 "50" I L 1700 4400 50 
F6 "30" I L 1700 4500 50 
F7 "15" I L 1700 4600 50 
$EndSheet
Wire Wire Line
	1650 2950 1600 2950
Wire Wire Line
	1600 2950 1600 4200
Wire Wire Line
	1600 4200 1700 4200
Wire Wire Line
	1650 3050 1550 3050
Wire Wire Line
	1550 3050 1550 4300
Wire Wire Line
	1550 4300 1700 4300
Wire Wire Line
	1650 3150 1500 3150
Wire Wire Line
	1500 3150 1500 4400
Wire Wire Line
	1500 4400 1700 4400
Wire Wire Line
	1650 3250 1450 3250
Wire Wire Line
	1450 3250 1450 4500
Wire Wire Line
	1450 4500 1700 4500
Wire Wire Line
	1650 3400 1400 3400
Wire Wire Line
	1400 3400 1400 4600
Wire Wire Line
	1400 4600 1700 4600
Wire Wire Line
	2300 2950 2500 2950
Wire Wire Line
	2500 2950 2500 4100
Wire Wire Line
	2500 4100 2300 4100
Wire Wire Line
	2500 2950 2500 1850
Wire Wire Line
	2500 1850 2300 1850
Connection ~ 2500 2950
Wire Wire Line
	1400 1500 1650 1500
Wire Wire Line
	1550 1950 1650 1950
$Comp
L SamacSys_Parts:PRT-00119 J4
U 1 1 60BA9294
P 1700 900
F 0 "J4" H 2100 1165 50  0000 C CNN
F 1 "PRT-00119" H 2100 1074 50  0000 C CNN
F 2 "SamacSys_Parts:PRT00119" H 2350 1000 50  0001 L CNN
F 3 "https://www.sparkfun.com/datasheets/Prototyping/Barrel-Connector-PJ-202A.pdf" H 2350 900 50  0001 L CNN
F 4 "SparkFun Accessories DC Barrel Power Jack/Connector" H 2350 800 50  0001 L CNN "Description"
F 5 "11" H 2350 700 50  0001 L CNN "Height"
F 6 "" H 2350 600 50  0001 L CNN "RS Part Number"
F 7 "" H 2350 500 50  0001 L CNN "RS Price/Stock"
F 8 "SparkFun" H 2350 400 50  0001 L CNN "Manufacturer_Name"
F 9 "PRT-00119" H 2350 300 50  0001 L CNN "Manufacturer_Part_Number"
	1    1700 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 60BA9EC7
P 1700 1000
F 0 "#PWR04" H 1700 750 50  0001 C CNN
F 1 "GND" H 1705 827 50  0000 C CNN
F 2 "" H 1700 1000 50  0001 C CNN
F 3 "" H 1700 1000 50  0001 C CNN
	1    1700 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 60BAA656
P 2500 900
F 0 "#PWR05" H 2500 650 50  0001 C CNN
F 1 "GND" H 2505 727 50  0000 C CNN
F 2 "" H 2500 900 50  0001 C CNN
F 3 "" H 2500 900 50  0001 C CNN
	1    2500 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 900  1400 1500
$Comp
L SamacSys_Parts:53261-0271 J2
U 1 1 60BBB079
P 650 2150
F 0 "J2" V 1146 1922 50  0000 R CNN
F 1 "53261-0271" V 1055 1922 50  0000 R CNN
F 2 "SamacSys_Parts:532610271" H 1400 2250 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/1707114" H 1400 2150 50  0001 L CNN
F 4 "1.25mm Pitch, PicoBlade PCB Header, Single Row, Right-Angle, Surface Mount, Tin (Sn) Plating, Friction Lock,  Circuits, Tape and Reel" H 1400 2050 50  0001 L CNN "Description"
F 5 "3.65" H 1400 1950 50  0001 L CNN "Height"
F 6 "1707114" H 1400 1850 50  0001 L CNN "RS Part Number"
F 7 "http://uk.rs-online.com/web/p/products/1707114" H 1400 1750 50  0001 L CNN "RS Price/Stock"
F 8 "Molex" H 1400 1650 50  0001 L CNN "Manufacturer_Name"
F 9 "53261-0271" H 1400 1550 50  0001 L CNN "Manufacturer_Part_Number"
	1    650  2150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	650  2150 650  2250
Wire Wire Line
	750  2150 1450 2150
Wire Wire Line
	1550 2150 1550 1950
$Comp
L power:GND #PWR02
U 1 1 60BA65AB
P 650 2250
F 0 "#PWR02" H 650 2000 50  0001 C CNN
F 1 "GND" H 655 2077 50  0000 C CNN
F 2 "" H 650 2250 50  0001 C CNN
F 3 "" H 650 2250 50  0001 C CNN
	1    650  2250
	1    0    0    -1  
$EndComp
NoConn ~ 650  1250
NoConn ~ 750  1250
Wire Wire Line
	1400 900  1700 900 
Wire Wire Line
	2300 3350 3250 3350
Wire Wire Line
	3250 3350 3250 3850
Wire Wire Line
	3250 3850 5050 3850
Wire Wire Line
	4400 2050 5050 2050
NoConn ~ 4800 2850
NoConn ~ 4400 2050
$Comp
L power:GND #PWR06
U 1 1 60BD1DE0
P 4850 4100
F 0 "#PWR06" H 4850 3850 50  0001 C CNN
F 1 "GND" H 4855 3927 50  0000 C CNN
F 2 "" H 4850 4100 50  0001 C CNN
F 3 "" H 4850 4100 50  0001 C CNN
	1    4850 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 60BD4B63
P 5750 4100
F 0 "#PWR07" H 5750 3850 50  0001 C CNN
F 1 "GND" H 5755 3927 50  0000 C CNN
F 2 "" H 5750 4100 50  0001 C CNN
F 3 "" H 5750 4100 50  0001 C CNN
	1    5750 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2150 1450 2600
Wire Wire Line
	1450 2600 3100 2600
Wire Wire Line
	3100 2600 3100 3100
Wire Wire Line
	3100 3100 2300 3100
Connection ~ 1450 2150
Wire Wire Line
	1450 2150 1550 2150
Wire Wire Line
	6050 1700 6050 2050
$Sheet
S 1650 1400 650  800 
U 60AADA82
F0 "Power Supply Subcircuit" 50
F1 "PSU.sch" 50
F2 "5V_out" O R 2300 1850 50 
F3 "Charger_in" I L 1650 1500 50 
F4 "Batt_in" I L 1650 1950 50 
F5 "V_reg_out" I R 2300 1700 50 
$EndSheet
Wire Wire Line
	2300 1700 6050 1700
$EndSCHEMATC
