EESchema Schematic File Version 4
LIBS:UPS-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C2
U 1 1 60BB46A2
P 1800 3100
F 0 "C2" H 1915 3146 50  0000 L CNN
F 1 "47u" H 1915 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1838 2950 50  0001 C CNN
F 3 "~" H 1800 3100 50  0001 C CNN
	1    1800 3100
	1    0    0    -1  
$EndComp
$Comp
L pspice:INDUCTOR L1
U 1 1 60BB486B
P 2250 2900
F 0 "L1" H 2250 3115 50  0000 C CNN
F 1 "100u" H 2250 3024 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2250 2900 50  0001 C CNN
F 3 "~" H 2250 2900 50  0001 C CNN
	1    2250 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2950 1800 2900
Wire Wire Line
	1800 2900 2000 2900
Wire Wire Line
	2500 2900 2550 2900
Wire Wire Line
	1800 3250 1800 3400
$Comp
L power:GND #PWR03
U 1 1 60BBB73A
P 2150 3400
F 0 "#PWR03" H 2150 3150 50  0001 C CNN
F 1 "GND" H 2155 3227 50  0000 C CNN
F 2 "" H 2150 3400 50  0001 C CNN
F 3 "" H 2150 3400 50  0001 C CNN
	1    2150 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3400 1800 3400
Text HLabel 4300 1600 0    50   Input ~ 0
Charger_in
Text HLabel 4150 2300 1    50   Input ~ 0
5V_out
Text HLabel 5500 1950 2    50   Input ~ 0
V_reg_out
Wire Wire Line
	1800 2900 1250 2900
Connection ~ 1800 2900
Text HLabel 1250 2900 1    50   Input ~ 0
Batt_in
$Comp
L SamacSys_Parts:HT7750A_SOT23-3 PS1
U 1 1 60C18851
P 2650 3000
F 0 "PS1" H 3200 2635 50  0000 C CNN
F 1 "HT7750A_SOT23-3" H 3200 2726 50  0000 C CNN
F 2 "SamacSys_Parts:SOT95P190X145-3N" H 3600 3100 50  0001 L CNN
F 3 "http://www.holtek.com.tw/documents/10179/11842/HT77xxAv140.pdf#search=%27HT7750A+SOT23%27" H 3600 3000 50  0001 L CNN
F 4 "PFM Step-up DC/DC Converter" H 3600 2900 50  0001 L CNN "Description"
F 5 "1.45" H 3600 2800 50  0001 L CNN "Height"
F 6 "" H 3600 2700 50  0001 L CNN "RS Part Number"
F 7 "" H 3600 2600 50  0001 L CNN "RS Price/Stock"
F 8 "Holtek" H 3600 2500 50  0001 L CNN "Manufacturer_Name"
F 9 "HT7750A SOT23-3" H 3600 2400 50  0001 L CNN "Manufacturer_Part_Number"
	1    2650 3000
	1    0    0    1   
$EndComp
Wire Wire Line
	2650 3000 2650 3400
Connection ~ 2150 3400
Wire Wire Line
	2150 3400 2650 3400
Wire Wire Line
	3750 3000 3750 2300
Wire Wire Line
	4250 2300 4250 2650
Wire Wire Line
	3750 2300 4250 2300
$Comp
L Device:C C1
U 1 1 60BB3AB3
P 4250 2800
F 0 "C1" H 4365 2846 50  0000 L CNN
F 1 "22u" H 4365 2755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4288 2650 50  0001 C CNN
F 3 "~" H 4250 2800 50  0001 C CNN
	1    4250 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3400 4250 3400
Wire Wire Line
	4250 3400 4250 2950
Connection ~ 2650 3400
$Comp
L SamacSys_Parts:1N5817-E3_73 D7
U 1 1 60C241EB
P 2550 2300
F 0 "D7" V 2804 2430 50  0000 L CNN
F 1 "1N5817-E3_73" V 2895 2430 50  0000 L CNN
F 2 "SamacSys_Parts:DIOAD1405W86L465D235" H 3000 2300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/1N5817-E3_73.pdf" H 3000 2200 50  0001 L CNN
F 4 "Schottky Diodes & Rectifiers Vr/20V Io/1A" H 3000 2100 50  0001 L CNN "Description"
F 5 "" H 3000 2000 50  0001 L CNN "Height"
F 6 "" H 3000 1900 50  0001 L CNN "RS Part Number"
F 7 "" H 3000 1800 50  0001 L CNN "RS Price/Stock"
F 8 "Vishay" H 3000 1700 50  0001 L CNN "Manufacturer_Name"
F 9 "1N5817-E3/73" H 3000 1600 50  0001 L CNN "Manufacturer_Part_Number"
	1    2550 2300
	0    1    1    0   
$EndComp
Connection ~ 2550 2900
Wire Wire Line
	2550 2900 2650 2900
Wire Wire Line
	2550 2300 3750 2300
Connection ~ 3750 2300
$Comp
L Device:R R19
U 1 1 60C2B0A3
P 4400 2300
F 0 "R19" V 4193 2300 50  0000 C CNN
F 1 "1" V 4284 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4330 2300 50  0001 C CNN
F 3 "~" H 4400 2300 50  0001 C CNN
	1    4400 2300
	0    1    1    0   
$EndComp
Connection ~ 4250 2300
$Comp
L SamacSys_Parts:MBR735 D9
U 1 1 60C2B929
P 5250 2300
F 0 "D9" H 5650 2033 50  0000 C CNN
F 1 "MBR735" H 5650 2124 50  0000 C CNN
F 2 "SamacSys_Parts:TO508P470X1050X2018-2P" H 5750 2450 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/6882088" H 5750 2350 50  0001 L CNN
F 4 "Taiwan Semi MBR735, Schottky Diode, 35V 7.5A, 2-Pin TO-220AC" H 5750 2250 50  0001 L CNN "Description"
F 5 "4.7" H 5750 2150 50  0001 L CNN "Height"
F 6 "6882088" H 5750 2050 50  0001 L CNN "RS Part Number"
F 7 "http://uk.rs-online.com/web/p/products/6882088" H 5750 1950 50  0001 L CNN "RS Price/Stock"
F 8 "Taiwan Semiconductor" H 5750 1850 50  0001 L CNN "Manufacturer_Name"
F 9 "MBR735" H 5750 1750 50  0001 L CNN "Manufacturer_Part_Number"
	1    5250 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	5150 2300 5500 2300
$Comp
L SamacSys_Parts:MBR735 D8
U 1 1 60C35385
P 5250 1600
F 0 "D8" H 5650 1333 50  0000 C CNN
F 1 "MBR735" H 5650 1424 50  0000 C CNN
F 2 "SamacSys_Parts:TO508P470X1050X2018-2P" H 5750 1750 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/6882088" H 5750 1650 50  0001 L CNN
F 4 "Taiwan Semi MBR735, Schottky Diode, 35V 7.5A, 2-Pin TO-220AC" H 5750 1550 50  0001 L CNN "Description"
F 5 "4.7" H 5750 1450 50  0001 L CNN "Height"
F 6 "6882088" H 5750 1350 50  0001 L CNN "RS Part Number"
F 7 "http://uk.rs-online.com/web/p/products/6882088" H 5750 1250 50  0001 L CNN "RS Price/Stock"
F 8 "Taiwan Semiconductor" H 5750 1150 50  0001 L CNN "Manufacturer_Name"
F 9 "MBR735" H 5750 1050 50  0001 L CNN "Manufacturer_Part_Number"
	1    5250 1600
	-1   0    0    1   
$EndComp
Wire Wire Line
	4550 1600 4300 1600
Wire Wire Line
	5150 1600 5500 1600
Wire Wire Line
	5500 1600 5500 2300
$EndSCHEMATC
