# 3088 Semester Project - Raspberry Pi Zero Uninterruptible Power Supply (UPS)

This repo contains KiCAD files for a Pi uHat to act as a power management system for project implementing the raspberry Pi Zero.

![Board_Front_View](/uploads/ea69c8135166d8ac299f2a0ba3ddedc4/Board_Front_View.PNG)

This goal of this project is to design a (micro) Hat for a raspberry pi zero which will act as an uninterruptible power supply. 
Such a device has many use cases and greatly expands the number of situations where the pi zero can be used. This is especially true for internet of things (IoT) applications in countries like south africa 
where a realiable source of mains power cannot be guarenteed. This project also aims to produce the hat in such a way as to require little to no management from the host pi. The idea is that the user loads the (very very minimal) programme to the pi which manages the shutdown procedure in the situation where the battery is depleted, then connects the board to the pi and uses it for whatever application they wish

The rest of the information on the device can be found in the main  [Design Report](https://gitlab.com/dylan_white/3088-semester-project/-/blob/master/Design%20Report.pdf)
