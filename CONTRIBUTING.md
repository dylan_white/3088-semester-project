![battery](/uploads/fcaeed289873249520a4791cfbbe1205/battery.jpg)

We would like to have a way to mount the battery we have designed around to the board. 
This would ideally be in the form of a 3D printable mount which would attach to the mounting holes.  

Information on both the dimensions of both the board and the battery can be found in this repo

Anybody who is comfortable with CAD software and wishes to assist with this is more than welcome to get in touch or make a pull request. 
