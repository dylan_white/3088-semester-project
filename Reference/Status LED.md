Led interpretation:

![Board_Front_View](/uploads/ea69c8135166d8ac299f2a0ba3ddedc4/Board_Front_View.PNG)


Interpretation of the LEDs should be fairly self explanatory due to the text on the surface of the PCB as seen above but the following points will explain when each LED will be illuminated.

- The first and second LED [blue] will turn on when the power is on and at 100% capacity.
- The third LED will be on when the power is at 100% and 80%
- the fourth led will be on when the power is at 100%, 80% and 50%.
- The fifth LED will be on when the battery is at 100%, 80%, 50% and 30%. 
- The sixth LED will be on when the power is at 100%, 80%, 50%, 30%, 15%.
